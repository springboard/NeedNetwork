CREATE TABLE `connections` (
	`id` INT(11) NOT NULL,
	`connectionname` VARCHAR(255) NULL DEFAULT NULL,
	`mobilenumber` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
