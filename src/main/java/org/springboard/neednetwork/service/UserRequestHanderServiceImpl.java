package org.springboard.neednetwork.service;

import org.springboard.neednetwork.dao.UserDAO;
import org.springboard.neednetwork.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRequestHanderServiceImpl implements UserRequestHanderService {
	
	@Autowired
	UserDAO userDao;

	@Override
	public String getUsers() {
		return userDao.getAll().toString();
	}

	@Override
	public String createUser(String username, String password) {
		User user = new User();
		user.setName(username);
		user.setPassword(password);
		return userDao.createUser(user);
	}

}
