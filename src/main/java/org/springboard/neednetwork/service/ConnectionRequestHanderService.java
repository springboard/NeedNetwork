package org.springboard.neednetwork.service;

public interface ConnectionRequestHanderService {

	String getConnections();

	String createConnection(String connectionname, String mobilenumber);

}