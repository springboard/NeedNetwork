package org.springboard.neednetwork.service;

public interface NeedcreationRequestHanderService {

	String getNeedCreations();

	String createNeedCreations(String NeedTitle, String NeedDescription,String NeadGoal,String FundingDuration,String NeedLocation);

}
