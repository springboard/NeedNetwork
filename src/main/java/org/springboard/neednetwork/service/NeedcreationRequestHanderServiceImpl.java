package org.springboard.neednetwork.service;

import org.springboard.neednetwork.dao.NeedcreationDAO;

import org.springboard.neednetwork.model.Needcreation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NeedcreationRequestHanderServiceImpl implements NeedcreationRequestHanderService {
	
	@Autowired
	NeedcreationDAO needcreationDao;
	
	
	@Override
	public String getNeedCreations() {
		return needcreationDao.getAll().toString();
	}
	
	@Override
	public String createNeedCreations( String NeedTitle, String NeedDescription, String NeadGoal, String FundingDuration, String NeedLocation ) {
		

 
		Needcreation needcreations = new Needcreation();
		needcreations.setTitle(NeedTitle);
		needcreations.setDescription(NeedDescription);
		needcreations.setGoal(NeadGoal);
		needcreations.setDuration(FundingDuration);
		needcreations.setLocation(NeedLocation);
		return needcreationDao.createNeedcreations(needcreations);
	}

	
}
