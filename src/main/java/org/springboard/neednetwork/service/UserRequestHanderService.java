package org.springboard.neednetwork.service;

public interface UserRequestHanderService {

	String getUsers();

	String createUser(String username, String password);

}
