package org.springboard.neednetwork.service;

import org.springboard.neednetwork.dao.ConnectionDAO;
import org.springboard.neednetwork.model.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConnectionRequestHanderServiceImpl implements ConnectionRequestHanderService {
	
	@Autowired
	ConnectionDAO connectionDao;

	@Override
	public String getConnections() {
		return connectionDao.getAll().toString();
	}

	@Override
	public String createConnection(String connectionname, String mobilenumber) {
		Connection connection = new Connection();
		connection.setName(connectionname);
		connection.setPassword(mobilenumber);
		return connectionDao.createConnection(connection);
	}

}
