package org.springboard.neednetwork.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springboard.neednetwork.model.Needcreation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("NeedcreationDAO")
public class NeedcreationDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

    @Transactional
	public List<Needcreation> getAll() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Needcreation> criteriaQuery = criteriaBuilder.createQuery(Needcreation.class);
		Root<Needcreation> from = criteriaQuery.from(Needcreation.class);
		CriteriaQuery<Needcreation> select = criteriaQuery.select(from);
		TypedQuery<Needcreation> typedQuery = session.createQuery(select);
		List<Needcreation> needcreations = typedQuery.getResultList();
		return needcreations;
	}

	@Transactional
	public String createNeedcreations(Needcreation needcreation) {
		// insert into database & return primary key
		int connectionId = (Integer) sessionFactory.getCurrentSession().save(needcreation);
		return "Connection information saved successfully with id " + connectionId;
	}

}
