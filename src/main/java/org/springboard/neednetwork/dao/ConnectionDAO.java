package org.springboard.neednetwork.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springboard.neednetwork.model.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("connectionDAO")
public class ConnectionDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

    @Transactional
	public List<Connection> getAll() {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
		CriteriaQuery<Connection> criteriaQuery = criteriaBuilder.createQuery(Connection.class);
		Root<Connection> from = criteriaQuery.from(Connection.class);
		CriteriaQuery<Connection> select = criteriaQuery.select(from);
		TypedQuery<Connection> typedQuery = session.createQuery(select);
		List<Connection> connections = typedQuery.getResultList();
		return connections;
	}

	@Transactional
	public String createConnection(Connection connection) {
		// insert into database & return primary key
		int connectionId = (Integer) sessionFactory.getCurrentSession().save(connection);
		return "Connection information saved successfully with id " + connectionId;
	}

}
