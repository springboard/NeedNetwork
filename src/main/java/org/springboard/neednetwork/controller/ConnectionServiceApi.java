package org.springboard.neednetwork.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springboard.neednetwork.service.ConnectionRequestHanderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/connections")
public class ConnectionServiceApi {
	@Autowired
	ConnectionRequestHanderService connectionServiceHandler;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllConnections() {
		return connectionServiceHandler.getConnections();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllConnections(@FormParam("firstname") String connectionname, @FormParam("lastname") String mobilenumber) {
		return connectionServiceHandler.createConnection(connectionname, mobilenumber);
	}
}
