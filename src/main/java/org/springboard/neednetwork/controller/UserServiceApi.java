package org.springboard.neednetwork.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springboard.neednetwork.service.UserRequestHanderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/users")
public class UserServiceApi {
	@Autowired
	UserRequestHanderService userServiceHandler;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllUsers() {
		return userServiceHandler.getUsers();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllUsers(@FormParam("username") String username, @FormParam("password") String password) {
		return userServiceHandler.createUser(username, password);
	}
}
