package org.springboard.neednetwork.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springboard.neednetwork.service.NeedcreationRequestHanderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/connections")
public class NeedCreationServiceApi {
	@Autowired
	NeedcreationRequestHanderService needcreationServiceHandler;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllConnections() {
		return needcreationServiceHandler.getNeedCreations();
	}

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllConnections(@FormParam("NeedTitle") String NeedTitle, @FormParam("NeedDescription") String NeedDescription,@FormParam("NeedGoal") String NeedGoal,@FormParam("FundingDuration") String FundingDuration,@FormParam("NeedLocation") String NeedLocation) {
		return needcreationServiceHandler.createNeedCreations(NeedTitle, NeedDescription,NeedGoal,FundingDuration,NeedLocation);
	}
}
