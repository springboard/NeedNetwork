package org.springboard.neednetwork.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "needcreations")
public class Needcreation implements Serializable {

	

	/**
	 * 
	 */
	

	// member variables
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "NeedTitle")
	private String NeedTitle;

	@Column(name = "NeedDescription")
	private String NeedDescription;
	
	@Column(name = "NeedGoal")
	private String NeedGoal;
	
	@Column(name = "FundingDuration")
	private String FundingDuration;
	
	@Column(name = "NeedLocation")
	private String NeedLocation;

	// getters & setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return NeedTitle;
	}

	public void setTitle(String NeedTitle) {
		this.NeedTitle = NeedTitle;
	}

	public String getDescription() {
		return NeedDescription;
	}

	public void setDescription(String NeedDescription) {
		this.NeedDescription = NeedDescription;
	}
	public String getGoal() {
		return NeedGoal;
	}

	public void setGoal(String NeedGoal) {
		this.NeedGoal = NeedGoal;
	}
	public String getDuration() {
		return FundingDuration;
	}

	public void setDuration(String FundingDuration) {
		this.FundingDuration = FundingDuration;
	}
	public String getLocation() {
		return NeedLocation;
	}

	public void setLocation(String NeedLocation) {
		this.NeedLocation = NeedLocation;
	}
}
