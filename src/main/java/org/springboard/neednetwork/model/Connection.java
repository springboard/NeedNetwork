package org.springboard.neednetwork.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "connections")
public class Connection implements Serializable {

	

	// member variables
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "connectionname")
	private String connectionname;

	@Column(name = "mobilenumber")
	private String mobilenumber;

	// getters & setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return connectionname;
	}

	public void setName(String connectionname) {
		this.connectionname = connectionname;
	}

	public String getPassword() {
		return mobilenumber;
	}

	public void setPassword(String mobilenumber) {
		this.mobilenumber = mobilenumber;
	}
}
